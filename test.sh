test()
{
  if [ "$2" == "$3" ]; then
    echo "Test $1 passed:"
    echo "Expected '$3' output, and it was '$2'"
  else
    echo "Test $1 failed:"
    echo "Expected '$3' output, but it was '$2'"
  fi
}

outputs=(
    "`echo "ITMO" | ./main 2> /dev/null`"
    "`echo "web" | ./main 2> /dev/null`"
    "`echo "TestTestTest" | ./main 2> /dev/null`"
    "`echo "LowLevelProgramming" | ./main 2>&1 > /dev/null`"
    "`echo "asssssssssssssssssssssssssssssssasssssssssssssssssssssssssssssssasssssssssssssssssssssssssssssssasssssssssssssssssssssssssssssssasssssssssssssssssssssssssssssssasssssssssssssssssssssssssssssssasssssssssssssssssssssssssssssssasssssssssssssssssssssssssssssssembler" | ./main 2>&1 > /dev/null`"
)

expectedResults=(
    "institut teplyh muzskih otnoshenii"
    "Web programming"
    "Test test Test"
    "Key was not found in dictionary"
    "Your input key has caused buffer overflow"
)

for i in "${!outputs[@]}"; do
    test "#$i" "${outputs[$i]}" "${expectedResults[$i]}"
done