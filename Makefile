ASM=nasm
ASMFLAGS=-felf64
LD=ld

%.o: %.asm 
	$(ASM) $(ASMFLAGS) -o $@ $< 

main: main.o lib.o dict.o
	$(LD) -o $@ $^


clean:
	rm -rf *.o 2>/dev/null
	rm -f main 2>/dev/null

test:
	bash test.sh
