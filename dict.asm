%include "lib.inc"
global find_word
global get_value_by_key

%define POINTER_SIZE 8
%define TRUE 1

section .text


; args:
;       rdi - buffer pointer
;       rsi - dictionary pointer
; returns:
;       rax - adress of word in dictionary if found, else 0
find_word:
    push r12
    push r13
    .loop:
        test rsi, rsi
        jz .not_found

        mov r12, rdi
        mov r13, rsi
        lea rsi, [rsi + POINTER_SIZE]
        call string_equals
        mov rdi, r12
        mov rsi, r13
        cmp rax, TRUE
        jz .found
        mov rsi, [rsi]
        jmp .loop
    .found:
        pop r13
        pop r12
        mov rax, rsi
        ret
    .not_found:
        pop r13
        pop r12
        xor rax, rax
        ret


get_value_by_key:
	add rdi, POINTER_SIZE
    push rdi
	call string_length
    pop rdi
	add rdi, rax
	inc rdi
	mov rax, rdi
	ret




