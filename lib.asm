global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error_message




%define SYS_EXIT 60
%define SYS_OUT 1
%define SYS_IN 0
%define SYS_WRITE 1
%define SYS_READ 0
%define SYS_ERR 2


%define STR_END 0
%define SINGLE_CHAR 1
%define NEW_LINE_CHAR 0xA
%define DECIMAL_ASCII_OFFSET 48
%define DECIMAL_BASE 10
%define TRUE 1
%define MINUS_SYMBOL_ASCII 45
%define PLUS_SYMBOL_ASCII 43
%define SPACE_SYMBOL 0x20
%define TAB_SYMBOL 0x9
%define MIN_WORD_LENGTH 2
%define DECIMAL_ASCII_LOWER_BOUND 48
%define DECIMAL_ASCII_UPPER_BOUND 57
%define BINARY_BASE 2

section .text
 
; Принимает код возврата и завершает текущий процесс
; args:
;       rdi = error_code
exit:
    mov rax, SYS_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
; args:
;       rdi - null-terminated string pointer
; returns:
;       rax - string length
string_length:
    xor rax, rax; setting rax value to zero
    .foreach:
        cmp byte[rdi + rax], STR_END ; Comparing current char to null terminator
        jz .foreach_end ; If current char is null terminator jumping to the end of foreach
        inc rax ; Incrementing counter(rax)
        jmp .foreach ; Next iteration
    .foreach_end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
; args:
;       rdi - null-terminated string pointer
print_string:
    .string_length_with_null_terminator:
        push rdi ; Saving rdi
        call string_length ; Counting string length(without null terminator)
        pop rsi ; Loading rdi
    .prepare_syscall:
        mov rdx, rax ; Moving rax to rdx - string size(see system-calls.md)
        mov rax, SYS_WRITE ; Moving syscall number to rax
        mov rdi, SYS_OUT ; Moving stdout descriptor to rax
    .end:
        syscall
        ret

; Принимает код символа и выводит его в stdout
; args:
;       rdi - char code
print_char:
    push rdi ; Pushing rdi to stack
    .prepare_syscall:
        mov rax, SYS_WRITE ; Moving stdout descriptor to rax
        mov rdi, SYS_OUT ; Moving stdout descriptor to rax
        mov rsi, rsp ; Moving stack pointer to rsi as pointer to string
        mov rdx, SINGLE_CHAR ; Moving buf size to rdx
    .end:
        syscall
        pop rax ; Popping char from stack
        ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEW_LINE_CHAR ; Moving new line char to rdi
    call print_char ; printing new line char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
; args:
;       rdi - unsigned int64_t
print_uint:
    mov r8, DECIMAL_BASE ; Move decimal base to r8 for further division operations
    mov rax, rdi ; moving int64_t to rax4

    
    push STR_END ; Put null terminator to stack

    .convert_to_decimal:
        xor rdx, rdx ; Clear rdx before division
        div r8
        .convert_to_ascii:
            add rdx, DECIMAL_ASCII_OFFSET
            push rdx 
        test rax, rax
        jnz .convert_to_decimal
    .print_char:
        pop rdi
        cmp rdi, STR_END
        jz .end
        call print_char ; printing result unsigned int8_t
        jmp .print_char        
    .end:
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
; args:
; rdi - int64_t
print_int:
    test rdi, rdi
    jge .print_uint
    .convert_to_direct_code:
        push rdi ; Save rdi in stack
        ; Print minus char
        mov rdi, MINUS_SYMBOL_ASCII
        call print_char
        pop rdi
        neg rdi ; Convert to direct code
    .print_uint:
        jmp print_uint ; Print unsigned int

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
; args:
;       rdi - first string pointer
;       rsi - second string pointer
; returns:
;       rax - 1 if str_1 equals str_2, 0 if not
string_equals:
    push rdi
    push rsi

    call string_length

    pop rdi
    push rax
    push rdi

    call string_length

    pop rsi
    pop r8
    pop rdi    

    cmp rax, r8
    jnz .fail
    xor rax, rax
    .compare_every_symbol:
        mov r9b, byte[rdi + rax]
        mov r10b, byte[rsi + rax]
        cmp r9b, r10b
        jnz .fail
        inc rax
        cmp rax, r8
        jg .end
        jmp .compare_every_symbol
    .fail:
        xor rax, rax
        ret
    .end:
        mov rax, TRUE
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    ; Prepare syscall (See system-calls.md)
    mov rax, SYS_READ ; mov syscall number
    mov rdi, SYS_IN ; mov file descriptor
    push rax ; Push trash into stack
    mov rsi, rsp 
    mov rdx, SINGLE_CHAR
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
; args:
;       rdi - buffer address
;       rsi - buffer size
; returns:
;       rax - 0 if operation was not completed correctly, else buffer address  
;       rdx - undefined if operation was not completed correctly, else word size
read_word:
    ; Save regs
    push r12
	push r13
	push r14
	xor r13, r13
	mov r14, rdi
	mov r12, rsi
	.check_space_symbols:
		call read_char
		cmp rax, SPACE_SYMBOL		
		jz .space
		cmp rax, TAB_SYMBOL
		jz .space
		cmp rax, NEW_LINE_CHAR
		jz .space
	.loop:	
		cmp rax, NEW_LINE_CHAR
		jz .end
		mov [r14 + r13], al
		cmp r13, r12
		jz .not_correct
		inc r13
		jmp .check_space_symbols
	.space:
		test r13, r13
		jz .check_space_symbols
		jmp .loop
	.not_correct: 	
        xor rax, rax
		jmp .ret
	.end: 	
		mov byte[r14 + r13], STR_END
		mov rax, r14
		mov rdx, r13
	.ret:	
		pop r14
		pop r13
		pop r12
		ret

 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
; args:
;       rdi - pointer to string
; returns:
;       rax - parsed unsigned int64_t if successfully parsed, else undefined
;       rdx - source number length if parsed, else 0
parse_uint:
    ; Clear registers
    xor rdx, rdx
    xor rax, rax
    .loop:
        mov r8b, byte[rdi + rdx] ; read char
        .check_end_loop: ; No need to check for null terminator because 0 < DECIMAL_ASCII_LOWER_BOUND
            cmp r8b, DECIMAL_ASCII_LOWER_BOUND
            jl .end
            cmp r8b, DECIMAL_ASCII_UPPER_BOUND
            jg .end
        .parsing:
            sub r8b, DECIMAL_ASCII_OFFSET ; Remove ascii offset
            imul rax, DECIMAL_BASE ; Shifting
            add rax, r8 ; Adding digit to number
        .next_iteration:
            inc rdx 
            jmp .loop
    .end:
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
; args:
;       rdi - string pointer
; returns:
;       rax - converted number if parsed, else undefined
;       rdx - 0 if not parsed, else string length
parse_int:
    xor rdx, rdx
    cmp byte[rdi], PLUS_SYMBOL_ASCII
    jz .parse_with_plus_sign
    cmp byte[rdi], MINUS_SYMBOL_ASCII
    jz .parse_with_minus_sing
    .parse_without_sign:
        jmp parse_uint ; Parsing like unsigned int
    .parse_with_plus_sign:
        inc rdi ; Increment rdi to avoid sign
        call parse_uint ; Parsing like unsigned int
        test rax, rax
        jz .end
        inc rdx ; Correcting rdx with adjusting sign char
        jmp .end
    .parse_with_minus_sing:
        inc rdi ; Increment rdi to avoid sign
        call parse_uint
        test rax, rax
        jz .end
        inc rdx ; Correcting rdx with adjusting sign char
        neg rax
    .end:
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; args:
;       rdi - string pointer
;       rsi - buffer pointer
;       rdx - buffer length (with null-terminator ?)
; returns:
;       rax - string length 1 if string successfully copied to buffer, 0 if string didn't fit into buffer
string_copy:
    ; Get string source string length
    push rdi
    push rsi
    push rdx
    
    call string_length

    pop rdx
    pop rsi
    pop rdi

    ; Сheck if there enough space for copying source string 
    cmp rdx, rax
    jbe .fail

    xor r8, r8
    .copy_loop:
        ; Move source string char to buf
        mov r9b, byte[rdi + r8]
        mov byte[rsi + r8], r9b
        ; If r8 == rax, source str ended, jmp to end
        cmp r8, rax
        jz .end
        ; Next interation
        inc r8
        jmp .copy_loop
    .fail:
        xor rax, rax ; Set rax = 0
        ret
    .end:
        ret


; args:
;       rdi - pointer to error message
print_error_message:
    .string_length_with_null_terminator:
        push rdi ; Saving rdi
        call string_length ; Counting string length(without null terminator)
        pop rsi ; Loading rdi
    .prepare_syscall:
        mov rdx, rax ; Moving rax to rdx - string size(see system-calls.md)
        mov rax, SYS_WRITE ; Moving syscall number to rax
        mov rdi, SYS_ERR ; Moving stdout descriptor to rax
    .end:
        syscall
        ret